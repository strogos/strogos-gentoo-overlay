EAPI=5
inherit eutils autotools git-r3

DESCRIPTION="a CUDA accelerated litecoin mining application based on pooler's CPU miner"
HOMEPAGE="https://github.com/cbuchner1/CudaMiner"
EGIT_REPO_URI="https://github.com/cbuchner1/CudaMiner.git"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS=""
IUSE="sm_52"

RDEPEND=">=dev-util/nvidia-cuda-toolkit-5.5.0"
DEPEND="${RDEPEND} >=dev-util/nvidia-cuda-sdk-5.5.0"

DOCS=( README.txt LICENSE.txt )

src_prepare() {
	ewarn "NOTE: YOU WILL MANUALLY HAVE TO PRODUCE A PATCH (or do a sed) UPDATING ALL REFERENCES"
	ewarn "TO YOUR GPU Compute Capabilities; With GTX 970=Compute Capability 5.2."
	ewarn "As such I have a custom use flag which applies a patch"
	ewarn "changing all instances (about 5 places) in the 'Makefile' (there are prob more ref: do a grep)"
	ewarn "referring to: e.g. USE=sm_52-> replace all instances of 'compute_10' to 'sm_52'"
	ewarn "(if it had Compute Capability 2.0, I would use use sm_20)"
	ewarn "-------------------------------------------------------------------------------"
	ewarn "ALSO: as of 2017-07-06; source does not compile with >gcc5!!"
	ewarn "USE ccminer instead: fork of cudaminer (cudaminer seems dead)!!"
	
	
	if use sm_52; then
		local PATCHES=(
			"${FILESDIR}"/${P}-sm_52-gtx970.patch			
		)
		epatch "${PATCHES[@]}"
		epatch_user
	fi
	
	sed -i "s#@CFLAGS@#-O3#g" Makefile.am || die
	eautoreconf
}

src_configure() {
	econf --with-cuda=/opt/cuda
}
