EAPI=6
inherit eutils autotools git-r3

DESCRIPTION="a CUDA accelerated litecoin mining application based on pooler's CPU miner"
HOMEPAGE="https://github.com/tpruvot/ccminer"
EGIT_REPO_URI="https://github.com/tpruvot/ccminer.git"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS=""
IUSE="sm_52 sm_61"
USE="${USE}"
#IUSE=""

RDEPEND=">=dev-util/nvidia-cuda-toolkit-6.5.0"
DEPEND="${RDEPEND} >=dev-util/nvidia-cuda-sdk-6.5.0"

DOCS=( README.txt LICENSE.txt )

src_prepare() {
	ewarn "NOTE: YOU WILL MANUALLY HAVE TO PRODUCE A PATCH (or do a sed); seems to work well with sm_52 out of the box;"
	ewarn "TO YOUR GPU Compute Capabilities; With GTX 970=Compute Capability 5.2."	
	ewarn "UPDATING ALL REFERENCES: change all instances (about 5 places) in the 'Makefile' (there are prob more ref: do a grep)"
	ewarn "referring to: e.g. USE=sm_52-> replace all instances of 'compute_x to 'sm_52'?"
	ewarn "(if it had Compute Capability 2.0, I would use use sm_20)?"
	ewarn "-------------------------------------------------------------------------------"
	ewarn "ALSO: as of 2017-06-07; source does not compile with >gcc5!!"
	ewarn "USING ccminer : its a fork of cudaminer (cudaminer is dead)!!"
	ewarn "#currently seems to bee defaulted on cuda_arch sm_52; keep an eye on this though"
	
	#currentlY seems to bee defaulted on cuda_arch sm_52; keep an eye on this though
	## update: had to re-enable in IUSE !!
# 	if use sm_52; then
#		eapply	"${FILESDIR}"/${P}-sm_52-gtx970.patch			

#	use sm_52 && elog "sm_52 assumed to be enabled by default"
#	fi
	
#	 if use sm_21; then
#		use sm_21 && eapply	"${FILESDIR}"/${P}-sm_21-gtx5xxx.patch || die
#	fi	

#	epatch "${PATCHES[@]}"

	## WHY THE HELL IS ALL PATCHES IN  FILESDIR AUTOMATICALLY DEPLOYED!!!!!!!!
	eapply	"${FILESDIR}"/${P}-custom-sm_xx-support.patch || die
	eapply_user

#	sed -i "s#@CFLAGS@#-O3#g" Makefile.am || die
#	./autogen.sh
	eautoreconf
}

src_configure() {
#	./configure --with-cuda=/opt/cuda
	econf --with-cuda=/opt/cuda
}
