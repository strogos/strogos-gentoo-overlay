My own collection of ebuilds
---

Note that I'm not a Gentoo developer, so none of these ebuilds are guaranteed to work. 
(...most of them are very dirty; e.g. just get something to work quickly, however clean-ups and contributions are welcome).
Issues can be submittet on [the issue tracker](https://bitbucket.org/strogos/gentoo-overlay/issues?status=new&status=open).

Pull Requests are also welcome...

Usage
-----

You can use this overlay by adding a file to `/etc/portage/repos.conf/` containing the following;
```
[strogos-gentoo-overlay]
priority = 50
location = /home/strogos/workspace/overlays/strogos-gentoo-overlay
sync-type = git
sync-uri = git@bitbucket.org:strogos/strogos-gentoo-overlay.git
auto-sync = Yes
```

Or just add it from layman [TODO: not implemented];
```
$ layman -a strogos
```


Why a local/custom Overlay (TODO: for reference, fix later and add to gentoo-g53sx notes repo!!)?
--------
If you only want to put one ebuild into your Portage tree and do not want to add a complete Overlay (like Sabayon, Gnome or KDE), this is your way. By using a local Overlay you can keep your stable Gentoo clean and have some unstable Packages from Overlays as well.

Creation
Code:	
```
$ sudo mkdir -p /usr/local/portage/overlay
$ sudo chown portage:portage /usr/local/portage/overlay	
```

Now go to http://gpo.zugaina.org/ and search for the ebuild you want to add to your Portage tree. Download something like "foo.ebuild". The ending .ebuild is important. For foo you now have to create the folders "category" and "foo" inside "category"
Code:	
```
sudo mkdir -p /usr/local/portage/overlay/category/foo
sudo cp /home/felix/Downloads/foo-2.1.0.47.ebuild /usr/local/portage/overlay/category/foo/
sudo chown -R portage:portage /usr/local/portage/overlay/*	
```

Now you have to create the manifest for this ebuild.
Code:	
```
$ sudo ebuild /usr/local/portage/overlay/category/foo/foo-2.1.0.47.ebuild manifest	
```

Last thing is to tell Portage that you have a local Overlay. Add the following to your /etc/make.conf:
Code:	

```
PORTDIR_OVERLAY="/usr/local/portage/overlay/"	
```

To give your local Overlay a name do following:
Code:	
```
$ mkdir -p /usr/local/portage/overlay/profiles
$ touch /usr/local/portage/overlay/profiles/repo_name
$ echo "my_overlay_name" >> /usr/local/portage/overlay/profiles/repo_name	
```

The foo version I used as an example is a stable one. If you add unstable ebuilds to your Portage tree, you have to unmask them in /etc/portage/package.unmask.
If your ebuild has dependencies that can not be resolved with the offical Portage tree, you have to add them to your local overlay as well.

