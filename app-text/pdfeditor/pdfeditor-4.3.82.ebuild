# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit xdg-utils desktop

MY_P="master-pdf-editor"
MY_V=4
DESCRIPTION="Create, convert and edit PDF and XPS files"
HOMEPAGE="http://code-industry.net/pdfeditor.php"
SRC_URI="http://get.code-industry.net/public/${MY_P}-${PV}_i386.tar.gz"

LICENSE="CodeIndustry"
SLOT="0"
KEYWORDS="x86"
IUSE=""

QA_PREBUILT="/opt/${PN}/pdfeditor"

DEPEND=">=dev-qt/qtcore-4.8.1:4"
RDEPEND="${DEPEND}
		media-gfx/sane-backends
		!app-text/master-pdf-editor"

S=${WORKDIR}/${MY_P}-${MY_V}

src_install() {
  insinto /opt/${PN}
  doins -r *
  fperms 755 /opt/${PN}/masterpdfeditor${MY_V}
  dosym /opt/${PN}/masterpdfeditor${MY_V} /opt/bin/pdfeditor  
#  dosym ../${PN}/masterpdfeditor4 /opt/bin/masterpdfeditor4
	make_desktop_entry "pdfeditor %f" \
		"Master PDF Editor ${PV}" /opt/${PN}/masterpdfeditor4.png \
		"Office;Graphics;Viewer" \
		"MimeType=application/pdf;application/x-bzpdf;application/x-gzpdf;\nTerminal=false"
  
  ewarn "This is the 32-bit version of ${PN} and it needs abi_x86_32 to be enabled by media-gfx/sane-backends pkg!"
  ewarn "TODO: This dependency is currently NOT handled by the ebuild!!"
  ewarn "FOR 64-bit: use  app-text/master-pdf-editor::gentoo instead! (64-bit-only)"
}

pkg_postinst() {
	xdg_desktop_database_update
}

pkg_postrm() {
	xdg_desktop_database_update
}
