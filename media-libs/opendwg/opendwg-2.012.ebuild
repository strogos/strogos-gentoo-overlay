# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# by karpi.web@email.cz
# thanx to #gentoo-dev


#IUSE="dwgdirect dwgtoolkit"
DESCRIPTION="OpenDWG Toolkit/Viewkit libraries"
HOMEPAGE="http://www.opendesign.com/"
RESTRICT="fetch"
KEYWORDS="~x86"
LIBARCHIVE="ad27linx.tar.gz"
LICENSE="GPL-2"
SLOT="0"

src_install() {
	einfo "You decided emerge binary opendwg libs into your system."
	einfo "At first you have to obtain OpenDWG Toolkit/Viewkit libraries"
	einfo "from ${HOMEPAGE}"
	einfo "If you've done, type path to your ${LIBARCHIVE}"
	einfo "for example: /home/name/just/downloaded/${LIBARCHIVE}"		
	read PKGPATH
	[ -f ${PKGPATH} ] || die "Error: ${PKGPATH} is not a regular file!"	
	mkdir -p ${D}/usr/lib/opendwg
	cd ${D}/usr/lib/opendwg	
	tar xvzf ${PKGPATH}
}
