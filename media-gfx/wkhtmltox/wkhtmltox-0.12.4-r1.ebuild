# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

inherit eutils

DESCRIPTION="Convert html to pdf (and various image formats) using webkit"
HOMEPAGE="http://wkhtmltopdf.org/ https://github.com/wkhtmltopdf/wkhtmltopdf/"
SRC_URI="https://github.com/wkhtmltopdf/wkhtmltopdf/archive/${PV}.tar.gz -> ${P}.tar.gz
	https://github.com/wkhtmltopdf/qt/archive/wk_4.8.7.zip -> qt-wk_4.8.7.zip"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 x86"
##note: libxml2 and Qt4 needs to either both be build with libicu or without.
IUSE="icu debug examples"

RDEPEND="!media-gfx/wkhtmltopdf
	media-libs/fontconfig
	x11-libs/libX11
	x11-libs/libXext
	x11-libs/libXrender
	icu? ( dev-libs/icu:0= )"

DEPEND="${RDEPEND}"

S="${WORKDIR}/wkhtmltopdf-${PV}"

##TODO: TypeTraits patch for >GCC v5 (e.g. std::tr1::has_trivial_constructor etc.. )
PATCHES=(
#	"${FILESDIR}/${PN}-4.8.5-honor-ExcludeSocketNotifiers-in-glib-event-loop.patch" # bug 514968
#	"${FILESDIR}/${PN}-4.8.5-qeventdispatcher-recursive.patch" # bug 514968
#	"${FILESDIR}/qt-wk-4.8.6-moc-boost-1.60.patch" # gentoo bug 556104
#	"${FILESDIR}/${PN}-4.8.7-libressl.patch" # bug 584796
)

src_unpack() {
	unpack ${A}
	mv "${WORKDIR}/"qt-wk_4.8.7/* "${S}"/qt || die
}


src_prepare() {
#	epatch "${PATCHES[@]}" || die

if use icu ; then
	sed -i -e '/CONFIG\s*+=\s*text_breaking_with_icu/ s:^#\s*::' \
		"${S}"/qt/src/3rdparty/webkit/Source/JavaScriptCore/JavaScriptCore.pri && 
	sed -i -e 's/-no-icu/-icu/g' \
		"${S}"/scripts/build.py
fi

#if use icu ; then
#	sed -i -e 's/-no-icu/-icu/g' \
			"${S}"/scripts/build.py
#fi

}


src_compile() {
	if use debug ; then
		scripts/build.py posix-local -debug
	else
		scripts/build.py posix-local
	fi
}

src_install() {
	if use debug ; then
		STATIC_PATH="${S}/static-build/posix-local-dbg/${P}"
	else
		STATIC_PATH="${S}/static-build/posix-local/${P}"
	fi

	insinto /usr
	dobin "${STATIC_PATH}"/bin/wkhtmltopdf
	dobin "${STATIC_PATH}"/bin/wkhtmltoimage
	insinto /usr/include
	doins -r "${STATIC_PATH}"/include
	insinto /usr/lib
	doins -r "${STATIC_PATH}"/lib
	dodoc AUTHORS CHANGELOG* README.md
	use examples && dodoc -r examples
}
