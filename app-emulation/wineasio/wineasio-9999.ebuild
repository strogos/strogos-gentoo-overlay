# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI="5"

inherit multilib-minimal git-r3

DESCRIPTION="ASIO driver for WINE"
HOMEPAGE="http://sourceforge.net/projects/wineasio"
#EGIT_REPO_URI="https://github.com/jhernberg/wineasio.git"
EGIT_REPO_URI="https://github.com/Jajcus/wineasio.git"
KEYWORDS=""
RESTRICT="mirror"
LICENSE="GPL-2"
IUSE=""
SLOT="0"

DEPEND="media-libs/asio-sdk"
RDEPEND="|| (
		app-emulation/wine-any
		app-emulation/wine
		virtual/wine
	)
	>=media-sound/jack-audio-connection-kit-1.9.10[${MULTILIB_USEDEP}]"

S="${WORKDIR}/${P}"

src_prepare() {
	#cp /opt/asiosdk2.3/ASIOSDK2.3/common/asio.h .
	cp /opt/asiosdk*/ASIOSDK*/common/asio.h .
	multilib_copy_sources
}

multilib_src_configure() {
	if has_multilib_profile && [[ ${ABI} == "amd64" ]] ; then
		mv Makefile64 Makefile
		./prepare_64bit_asio
	fi

	default
}

multilib_src_install() {
	exeinto /usr/$(get_libdir)/wine
	doexe *.so
}

pkg_postinst() {
	echo
	elog "Finally the dll must be registered in the wineprefix."
	elog "For both 32 and 64 bit wine do:"
	elog "# regsvr32 wineasio.dll"
	elog
	elog "On a 64 bit system with wine supporting both 32 and 64 applications, regsrv32"
	elog "will register the 32 bit driver in a 64 bit prefix, use the following command"
	elog "to register the 64 bit driver in a 64 bit wineprefix:"
	elog
	elog "# wine64 regsvr32 wineaiso.dll.so"
	elog
	elog "regsvr32 registers the ASIO COM object in the default prefix "~/.wine"."
	elog "To use another prefix specify it explicitly, like:"
	elog "# env WINEPREFIX=~/asioapp regsvr32 wineasio.dll.so"
	echo
	
		ewarn "Currently pulling source from a fork.. try the original one later (didn't compile.. and I dont have time)..."
		ewarn "See  https://sourceforge.net/p/wineasio/discussion/802003/thread/bfb99275/ for a discussion on this issue"
		ewarn "Remember that 64-bit prefix might still be problematic..."
		
	elog "NOTE... this ebuild doesn't consider the new wine packagages in gentoo (e.g. wine-any etc) "
	elog "Everything is installed to /usr/lib/wine and /usr/lib64/wine"
	elog "So for now you will have to manually symlink (or whatever) dll's to the type of wine pkg you are using"
	elog "I do as follows ln -s /usr/lib/wine/wineasio.dll.so /usr/lib/wine-any-3.21/. "
	elog "and ln -s /usr/lib64/wine/wineasio.dll.so /usr/lib64/wine-any-3.21/. (didn't work!!??)"
	elog "...or something like wine64-any-3.21 regsvr32 wineaiso.dll.so and  regsvr32-any-3.21 wineaiso.dll.so"
	
	ewarn "if it doesn't compile, remember to add abi_x86_32 use to jack "
}
