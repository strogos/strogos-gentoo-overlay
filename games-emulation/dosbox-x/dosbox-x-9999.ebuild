# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

##My openglide-patch for dosbox-x is based on the following:
#GLIDE_PATCH=3722fc563b737d2d7933df6a771651c2154e6f7b
#SRC_URI="glide? ( https://raw.githubusercontent.com/voyageur/openglide/${GLIDE_PATCH}/platform/dosbox/dosbox_glide.diff -> dosbox_glide-${GLIDE_PATCH}.diff )"


if [[ ${PV} = 9999 ]]; then
	#ESVN_REPO_URI="https://svn.code.sf.net/p/dosbox/code-0/dosbox/trunk"
	#inherit subversion
	EGIT_REPO_URI="https://github.com/joncampbell123/dosbox-x.git"
	inherit git-r3
	KEYWORDS=""
else
	##Note that dosbox-x maintainer's release tags/versioning is a complete mess. No guarantees here...
	SRC_URI+="https://github.com/joncampbell123/dosbox-x/archive/dosbox-x-macosx-x64-20160219-2130.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64 ~arm ~ppc ~ppc64 ~sparc ~x86"
fi

inherit autotools eutils flag-o-matic

DESCRIPTION="DOS emulator"
HOMEPAGE="http://dosbox-x.com/"

LICENSE="GPL-2"
SLOT="0"
#IUSE="alsa debug hardened opengl"
IUSE="alsa debug glide hardened opengl"

#pcap release have define flag HAVE_REMOTE to be defined when including pcap.h; this is a problem; but the tip of the libpcap master branch does not have that botch; hopefully
#future version releases of libpcap will make this an obsolete issue. As of date i'm using 9999 ebuild of pcap.
DEPEND="alsa? ( media-libs/alsa-lib )
	glide? ( media-libs/openglide )
	opengl? ( virtual/glu virtual/opengl )
	debug? ( sys-libs/ncurses:0 )
	media-libs/libpng:0
	>net-libs/libpcap-1.8.1-r1
	media-libs/libsdl[joystick,video,X]
	media-libs/sdl-net
	media-libs/sdl-sound"
RDEPEND=${DEPEND}

#if [[ ${PV} != 9999 ]]; then
#	S=${WORKDIR}/${PN}
#fi

#PATCHES=( "${FILESDIR}/${PN}-0.74-gcc46.patch" )

if [[ ${PV} != 9999 ]]; then
src_unpack() {
	ewarn "Crazy package maintainer; must rename build directory before build"
        unpack ${A}
	mv ${WORKDIR}/* ${WORKDIR}/${P} || die "renaming build dir failed";
}
fi


src_prepare() {
##	!openglide support (provided by patch) is experimental; untested actually...=D
#	use glide && eapply "${DISTDIR}"/dosbox_glide-${GLIDE_PATCH}.diff
	use glide && eapply "${FILESDIR}"/openglide-${P}.patch
	default
	eautoreconf
}

src_configure() {
	use glide && append-cppflags -I"${EPREFIX}"/usr/include/openglide
	append-cppflags -I"${EPREFIX}"/usr/include/openglide

	econf \
		$(use_enable alsa alsa-midi) \
		$(use_enable !hardened dynamic-core) \
		$(use_enable !hardened dynamic-x86) \
		$(use_enable debug) \
		$(use_enable opengl)
}

src_install() {
	default
	make_desktop_entry dosbox DOSBox /usr/share/pixmaps/dosbox.ico
	doicon src/dosbox.ico
}

pkg_postinst() {
	if use glide; then
		elog "Dosbox-x enables Glide emulation. To use this, symlink"
		elog "or copy ${EPREFIX}/usr/share/openglide/glide2x-dosbox.ovl to your game's"
		elog "directory and add the following to your DOSBox configuration."
		elog ""
		elog "[glide]"
		elog "glide=true"
	fi
}
