# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

#EAPI=5
EAPI=7
#these repos are prob dead [github mirror seems to be stuck at v1.9.95]
#ESVN_REPO_URI="https://pcsxr.svn.codeplex.com/svn/pcsxr"
#ESVN_PROJECT="pcsxr"
#EGIT_REPO_URI="https://github.com/mirror/pcsxr.git"
EGIT_REPO_URI="https://strogos@bitbucket.org/strogos/pcsxr.git"

inherit eutils autotools git-r3 multilib
#games
#subversion

DESCRIPTION="PCSX-Reloaded: a fork of PCSX, the discontinued Playstation emulator"
HOMEPAGE="http://pcsxr.codeplex.com"
SRC_URI=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE="alsa cdio openal ffmpeg nls opengl oss pulseaudio +sdl-sound +sdl"

RDEPEND="x11-libs/gtk+:3
	media-libs/libsdl
	sys-libs/zlib	
	x11-libs/libX11:0=
	x11-libs/libXext:0=
	x11-libs/libXtst:0=
	x11-libs/libXv:0=
	alsa? ( media-libs/alsa-lib:0= )
	cdio? ( dev-libs/libcdio:0= )
	ffmpeg? ( virtual/ffmpeg:0= )
	nls? ( virtual/libintl:0= )
	openal? ( media-libs/openal:0= )
	opengl? ( virtual/opengl:0=
		x11-libs/libXxf86vm:0= )
	pulseaudio? ( >=media-sound/pulseaudio-0.9.16 )
	cdio? ( dev-libs/libcdio )
	sdl? ( media-libs/libsdl:0=[sound] )"
	

DEPEND="${RDEPEND}
	!games-emulation/pcsx
	!games-emulation/pcsx-df
	dev-util/intltool
	x11-proto/videoproto
	nls? ( sys-devel/gettext:0 )
	x86? ( dev-lang/nasm )"

REQUIRED_USE="^^ ( alsa openal oss pulseaudio sdl-sound sdl )"

# it's only the .po file check that fails :)
RESTRICT=test

#set workdir
S=${WORKDIR}/${P}

src_prepare() {
	default
	#eapply_user
##prepatched in strogos repo!
#	local PATCHES=(
#		"${FILESDIR}"/${P}-zlib-uncompress2.patch
#		"${FILESDIR}"/${P}-ffmpeg3-and-libav.patch
#	)

#	epatch "${PATCHES[@]}"
#	epatch_user

	##can't get eautotools to work
	#eautoreconf
	./autogen.sh	
}

src_configure() {

	local sound_backend

	if use alsa; then
		sound_backend=alsa
	elif use oss; then
		sound_backend=oss
	elif use pulseaudio; then
		sound_backend=pulseaudio
	elif use sdl; then
		sound_backend=sdl
	elif use openal; then
		sound_backend=openal
	else
		sound_backend=null
	fi

	local myconf=(
		$(use_enable nls)
		$(use_enable cdio libcdio)
		$(use_enable opengl)
		$(use_enable ffmpeg ccdda)
		--enable-sound=${sound_backend}
	)
	
	##can't get eautotools to work
	#econf "${myconf[@]}"
	./configure "${myconf[@]}"
}

src_install() {	
	default
	prune_libtool_files --all

	dodoc doc/{keys,tweaks}.txt
}

pkg_postinst() {
	local vr
	for vr in ${REPLACING_VERSIONS}; do
		if ! version_is_at_least 1.9.94-r1 ${vr}; then
			ewarn "Starting with pcsxr-1.9.94-r1, the plugin install path has changed."
			ewarn "In order for pcsxr to find plugins, you will need to remove stale"
			ewarn "symlinks from ~/.pcsxr/plugins. You can do this using the following"
			ewarn "command (as your regular user):"
			ewarn
			ewarn " $ find ~/.pcsxr/plugins/ -type l -delete"
		fi
	done
}
