# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=7

inherit eutils git-r3 linux-mod udev
MY_P=digimend-kernel-drivers-${PV}

DESCRIPTION="Simple Interface remuxing MKV files for playback on ps3/ps4 systems"
HOMEPAGE="http://ps3muxer.org"

EGIT_REPO_URI="https://github.com/DIGImend/digimend-kernel-drivers.git"

#LICENSE="GPL-2"
LICENSE="GPL-3"
SLOT="0"
KEYWORDS=""
#IUSE="qt5"
#REQUIRED_USE="^^ ( qt5 )"

RDEPEND=">=sys-kernel/gentoo-sources-3.5"
#DEPEND="dev-qt/qtgui:5"
#S=${WORKDIR}/${MY_P}


#src_unpack() {
#    git-r3_src_unpack
#}

#src_configure() {
#    eqmake5 ps3muxer.pro
#}

#src_compile() {
	#eqmake5 ${S}/ps3muxer.pro && e
	#make DESTDIR="${D}"
#}

src_install() {
#	emake DESTDIR="${D}" INSTALL_ROOT="${D}" install

        linux-mod_src_install
#	cat > "${udevrules}" <<-EOF
#		KERNEL=="vmci",  GROUP="vmware", MODE="660"
#		KERNEL=="vmw_vmci",  GROUP="vmware", MODE="660"
#		KERNEL=="vmmon", GROUP="vmware", MODE="660"
#		KERNEL=="vsock", GROUP="vmware", MODE="660"
#	EOF
#	udev_dorules "${udevrules}"

}
