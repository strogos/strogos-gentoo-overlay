# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit eutils git-r3 qmake-utils
MY_P=ps3muxer-${PV}

DESCRIPTION="Simple Interface remuxing MKV files for playback on ps3/ps4 systems"
HOMEPAGE="http://ps3muxer.org"

EGIT_REPO_URI="https://strogos@bitbucket.org/strogos/ps3muxer.git"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""
IUSE="qt5"
REQUIRED_USE="^^ ( qt5 )"

RDEPEND="media-video/mkvtoolnix"
DEPEND="dev-qt/qtgui:5"
S=${WORKDIR}/${MY_P}


src_unpack() {
    git-r3_src_unpack
}

#src_configure() {
#    eqmake5 ps3muxer.pro
#}

#src_compile() {
	#eqmake5 ${S}/ps3muxer.pro && e
	#make DESTDIR="${D}"
#}

##TODO: not the prettiest method, but couldn't get eqmake5 to use the already existing ps3muxer.pro.
src_install() {
	#emake DESTDIR="${D}" INSTALL_ROOT="${D}" install
	qmake "${S}/ps3muxer.pro" && emake DESTDIR="${D}" INSTALL_ROOT="${D}" install	
}
